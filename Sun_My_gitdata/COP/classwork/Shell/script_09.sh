#!/bin/bash

# script to print pattern by using nested loops

clear

# accept no. of rows from user
echo -n "enter no. of rows : "
read n

i=0 
echo "output is : "

while [ $i -lt $n ] # outer while loop
do
    j=0
    while [ $j -lt `expr $n - $i` ] # inner while loop
    do
        echo -n " * "
        j=`expr $j + 1` #increment the value of j by 1
    done

    echo " "
    i=`expr $i + 1` #increment the value of i by 1
done

exit
