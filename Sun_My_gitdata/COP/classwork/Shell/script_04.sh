
#!/bin/bash

# calculate cirlcle area
# area =PI*r*r

# |pipe used to give op of one cmd as a input  to other cmd

# define PI as a constant 
PI=3.14

# accept radius from user
echo -n "enter the radius : "
read rad

# calculate area of the circle = PI*rad*rad
area=`echo "$PI * $rad * $rad" | bc`
echo "area of the circle is : $area sq. units"

exit
