#!/bin/bash

# it is a good programming practice to mention shebang line:

# script to display table of a number

clear

# accept number from user
echo -n "enter the number : "
read num

# to display table of a number by using while loop

# initialization
i=1

echo "while => table of $num is : "

# i<=10
while [ $i -le 10 ] #termination condition
do
    res=`expr $num \* $i`
    echo "$res"
    i=`expr $i + 1` # modification
done

# to display table of a number by using until loop

# initialization
i=1

echo "until => table of $num is : "
#upto 10 
until [ $i -gt 10 ] #termination condition
do
    res=`expr $num \* $i`
    echo "$res"
    i=`expr $i + 1` # modification
done

# to display table of a number by using for loop

echo "for => table of $num is : "


# for i in 1 2 3 4 5 6 7 8 9 10

#   like i=1; i<100;i++5;
for i in {1..100..5}
do
    res=`expr $num \* $i`
    echo "$res"
done


exit

