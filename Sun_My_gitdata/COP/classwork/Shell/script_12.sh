#!/bin/bash

# script to write a function to do addition of two numbers

function addition( )
{
    echo "inside function addition( )"
    res=`expr $1 + $2`  # $1 & $2 are formal params
    echo "res = $res"
}

clear

echo "execution of script $0 started !!!"

echo -n "enter n1 : "
read n1

echo -n "enter n2 : "
read n2

echo "way-1 : "

# way-1: result is getting printed inside function itself
addition $n1 $n2

echo "way-2 : "

# way-2: expected that function should return a value
sum=$( addition $n1 $n2 )
echo "sum = $sum"

# in way-1 function calling echo statement is used for printing purpose, whereas in way-2 function calling
# echo statement is used for stroing return value purpose


echo "execution of script $0 exited !!!"
exit





