#!/bin/bash

# script to write a function for printing table of a number

function print_table( )
{
    echo "table of $1 is : "
    for i in {1..10..1}
    do
        res=`expr $1 \* $i`
        echo "$res"
    done
}

clear

echo "script $0 started !!!"

echo -n "enter the number : "
read num

# way-1: echo statement is used for printing purpose
echo "way-1 : "
print_table $num

# way-2: echo statement is used for storing values
echo "way-2 : "
tab=$( print_table $num)
echo "tab is : $tab"

echo "script $0 exited !!!"
exit





