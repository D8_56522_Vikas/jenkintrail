#include<stdio.h>

int main(void){

    int n;
    int i;
    int j;

    printf("enter n : ");
    scanf("%d", &n);

    for( i = 0 ; i < n ; i++ ){
        for( j = 0 ; j < n-i ; j++ ){
            printf("%4s", "*");
        }
        printf("\n");
    }

    return 0;
}