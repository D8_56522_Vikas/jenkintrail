#!/bin/bash

# above line is called as a "shebang line", first line in a shell script program is called as  shebang line
# in which  we need to mention # symbol with ! sign followed by an absolute path of  shell program which 
# will going to execute  that script.

# in shell script program whatever is there in front of # (pragma) symbol is considered as a commment.

clear

echo -n "today's date is : "
date +"%d / %m / %Y"

echo "cal of cur month is : "
cal

exit # due to exit script will be exited