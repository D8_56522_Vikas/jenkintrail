#!/bin/bash

# script to do addition of two positional parameters

clear

echo "no. of pos params are : $#"

if [ $# -ne 2 ] #if pos params are not exactly equal to 2
then
    echo "invalid no. of pos params to the script : $0"
    exit    #due to exit script will be exited
fi

# if pos params are exactly equal to 2
sum=`expr $1 + $2`
echo "sum is : $sum"

exit

