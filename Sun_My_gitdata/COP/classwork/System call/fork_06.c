/*
    "zombie process":
    - if child process exits before its parent, child process goes into zombie state 
    - if child process exits before its parent, and its parent continues its execution without 
    reading an exit status of its child then entry of child process i.e. PCB of child remains present
     into the main memory even after exiting i.e. in defunt state such state of child process is also
    called as zombie state.
    ================================================================================================ */

#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>

int main(void)
{
    int ret;
    int i;

    printf("main() started !!!\n");
   
    ret = fork();


    if( ret == 0 )//child process
    {
        for( i = 1 ; i <= 20 ; i++ )
        {
            printf("child : %d\n", i);
            sleep(1);
        }
        printf("child exited !!!\n");
    }
    else//parent process
    {
        for( i = 1 ; i <= 40 ; i++ )
        {
            printf("parent : %d\n", i);
            sleep(1);
        }
        printf("parent exited !!!\n");
    }
    
    

    return 0;
}