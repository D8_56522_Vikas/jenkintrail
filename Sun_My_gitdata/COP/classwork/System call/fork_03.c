// Program to demonstrate fork() sys call

#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>


int main(void)
{
    int ret;
    
    printf("main() started !!!\n");
   
    printf("parent's parent pid  = %d\n", getppid());
    printf("parent pid = %d\n", getpid());

    ret = fork();

    //if fork() sys call fails
    if( ret == -1 )
    {
        //printf("fork() sys call failed !!!\n"); => prints only user's message
        perror("fork() sys call failed !!!\n");
        //perror() is c lib function which prints actual system error message with user's message
        _exit(0);//_exit() sys call used to terminate process
    }


    if( ret == 0 )//child process
    {
        printf("child : ret = %d\n", ret);
        printf("child's pid  = %d\n", getpid());
        printf("child's parent pid = %d\n", getppid());
    }
    else//parent process
    {
        printf("parent : ret = %d\n", ret);
        printf("parent's parent pid  = %d\n", getppid());
        printf("parent : pid = %d\n", getpid());
    }
    
    printf("main() exited !!!\n");

    return 0;
}
