/*  
    "orphan process":
    if parent process exits before its child process, then child process becomes orphan
    and ownership of orphan process will be taken by init process
================================================================================================ */

#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>

int main(void)
{
    int ret;
    int i;

    printf("main() started !!!\n");
   
    ret = fork();


    if( ret == 0 )//child process
    {
        for( i = 1 ; i <= 40 ; i++ )
        {
            printf("child : %d\n", i);
            sleep(1);
        }
        printf("child exited !!!\n");
    }
    else//parent process
    {
        for( i = 1 ; i <= 20 ; i++ )
        {
            printf("parent : %d\n", i);
            sleep(1);
        }
        printf("parent exited !!!\n");
    }
    
    

    return 0;
}

            