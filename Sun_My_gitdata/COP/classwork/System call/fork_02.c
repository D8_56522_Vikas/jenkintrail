// Program to demonstrate fork() sys call

#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>


int main(void)
{
    int ret;
    
    printf("main() started !!!\n");
   
    printf("parent : pid = %d\n", getpid());

    ret = fork();

    printf("ret = %d\n", ret);

    //printf("parent : pid = %d\n", getppid());
    //printf("pid : %d\n", getpid());


    printf("main() exited !!!\n");

    return 0;
}
